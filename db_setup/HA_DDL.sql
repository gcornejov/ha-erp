CREATE DATABASE ha_erp WITH ENCODING 'UTF8';

SELECT ha_erp;
 -- Gustavo
CREATE TABLE PRODUCTO (
	Producto_Id VARCHAR(15) NOT NULL PRIMARY KEY,				-- 15 Bytes
	Producto_Cd VARCHAR(5),										--  5 Bytes
	Producto_CdMd VARCHAR(10),									-- 10 Bytes
	Producto_Dsc VARCHAR(50),									-- 50 Bytes
	Producto_Fmt VARCHAR(5),									--  5 Bytes
	Producto_Prc INTEGER,										--  4 Bytes
	Producto_PtPd SMALLINT,										--  2 Bytes
	Producto_StRv SMALLINT,										--  2 Bytes
	Producto_CrDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP,	--  8 Bytes
	Producto_MdDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP		--  8 Bytes
);																-- Total: 109 Bytes

CREATE TABLE PROVEEDOR (
	Proveedor_Id VARCHAR(15) NOT NULL PRIMARY KEY,				--  15 Bytes
	Proveedor_Rut VARCHAR(15),									--  15 Bytes
	Proveedor_RnSl VARCHAR(100),								-- 100 Bytes
	Proveedor_Dir VARCHAR(100),									-- 100 Bytes
	Proveedor_Tel VARCHAR(15),									--  15 Bytes
	Proveedor_Eml VARCHAR(100),									-- 100 Bytes
	Proveedor_CrDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP,	--   8 Bytes
	Proveedor_MdDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP	--   8 Bytes
);																-- Total: 361

CREATE TABLE LISTASOLICITUD (
	ListaSolicitud_Id VARCHAR(15) NOT NULL PRIMARY KEY,				-- 10 Bytes
	ListaSolicitud_Dt TIMESTAMP, 									--  8 Bytes
	ListaSolicitud_Tp VARCHAR(10),									-- 10 Bytes
	ListaSolicitud_CrDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP,	--  8 Bytes
	ListaSolicitud_MdDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP	--  8 Bytes
);																	-- Total: 44 Bytes

CREATE TABLE ORDENCOMPRA (
	OrdenCompra_Id VARCHAR(15) NOT NULL PRIMARY KEY,										--  15 Bytes
	OrdenCompra_Num INTEGER,																--   4 Bytes
	OrdenCompra_Dt TIMESTAMP,																--   8 Bytes
	OrdenCompra_CndVnt VARCHAR(50),															--  50 Bytes
	OrdenCompra_SbTot INTEGER,																--   4 Bytes
	OrdenCompra_DsCnt INTEGER,																--   4 Bytes
	OrdenCompra_Neto INTEGER,																--   4 Bytes
	OrdenCompra_Iva INTEGER,																--   4 Bytes
	OrdenCompra_Tot INTEGER,																--   4 Bytes
	OrdenCompra_Obs VARCHAR(200),															-- 200 Bytes
	OrdenCompra_Est VARCHAR(8),																--   8 Bytes
	ListaSolicitud_Id VARCHAR(15) references LISTASOLICITUD(ListaSolicitud_Id) NOT NULL,	--  15 Bytes
	Proveedor_Id VARCHAR(15) references PROVEEDOR(Proveedor_Id) NOT NULL,					--  15 Bytes
	OrdenCompra_CrDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP,								--   8 Bytes
	OrdenCompra_MdDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP								--   8 Bytes
);																							-- Total: 351 Bytes

CREATE TABLE INCIDENCIA (
	Incidencia_Id VARCHAR(15) NOT NULL PRIMARY KEY,								--  15 Bytes
	Incidencia_Tp VARCHAR(10),													--  10 Bytes
	Incidencia_Dsc VARCHAR(100),												-- 100 Bytes
	Producto_Id VARCHAR(15) references PRODUCTO(Producto_Id) NOT NULL,			--  15 Bytes
	OrdenCompra_Id VARCHAR(15) references ORDENCOMPRA(OrdenCompra_Id) NOT NULL,	--  15 Bytes
	Incidencia_CrDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP,					--   8 Bytes
	Incidencia_MdDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP					--   8 Bytes
);																				-- Total: 171 Bytes

CREATE TABLE LOTE (
	Lote_Id VARCHAR(15) NOT NULL PRIMARY KEY,									-- 15 Bytes
	Lote_Num INTEGER,															--  4 Bytes
	Lote_DtRc TIMESTAMP,														--  8 Bytes
	Lote_Ps	INTEGER,															--  4 Bytes
	OrdenCompra_Id VARCHAR(15) references PRODUCTO(Producto_Id) NOT NULL,		-- 15 Bytes
	Producto_Id VARCHAR(15) references ORDENCOMPRA(OrdenCompra_Id) NOT NULL,	-- 15 Bytes
	Lote_CrDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP,						--  8 Bytes
	Lote_MdDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP							--  8 Bytes
);																				-- Total: 77 Bytes

CREATE TABLE UBICACION (
	Ubicacion_Id VARCHAR(15) NOT NULL PRIMARY KEY,							-- 15 Bytes
	Ubicacion_Pas VARCHAR(2),												--  2 Bytes
	Ubicacion_Col VARCHAR(2),												--  2 Bytes
	Ubicacion_Fil VARCHAR(2),												--  2 Bytes
	Ubicacion_CrDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP,				--  8 Bytes
	Ubicacion_MdDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP				--  8 Bytes
);																			-- Total: 37 Bytes

CREATE TABLE BULTO (
	Bulto_Id VARCHAR(15) NOT NULL PRIMARY KEY,								-- 15 Bytes
	Bulto_Tp VARCHAR(4),													--  4 Bytes
	Bulto_CntOg SMALLINT,													--  2 Bytes
	Bulto_Ps SMALLINT,														--  2 Bytes
	Bulto_VncDt TIMESTAMP,													--  8 Bytes
	Bulto_CntAct SMALLINT,													--  2 Bytes
	Producto_Id VARCHAR(15) references PRODUCTO(Producto_Id) NOT NULL,		-- 15 Bytes
	Lote_Id VARCHAR(15) references LOTE(Lote_Id) NOT NULL,					-- 15 Bytes
	Ubicacion_Id VARCHAR(15) references UBICACION(Ubicacion_Id) NOT NULL,	-- 15 Bytes
	Bulto_CrDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP,					--  8 Bytes
	Bulto_MdDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP					--  8 Bytes
);																			-- Total: 94 Bytes

CREATE TABLE BULTOENVASADO (
	BultoEnvasado_Id VARCHAR(15) NOT NULL PRIMARY KEY,						-- 15 Bytes
	BultoEnvasado_EnvDt TIMESTAMP,											--  8 Bytes
	BultoEnvasado_CntOg SMALLINT,											--  2 Bytes
	BultoEnvasado_Ps SMALLINT,												--  2 Bytes
	BultoEnvasado_VncDt TIMESTAMP,											--  8 Bytes
	BultoEnvasado_CntAct SMALLINT,											--  2 Bytes
	Producto_Id VARCHAR(15) references PRODUCTO(Producto_Id) NOT NULL,		-- 15 Bytes
	Ubicación_Id VARCHAR(15) references UBICACION(Ubicacion_Id) NOT NULL,	-- 15 Bytes
	BultoEnvasado_CrDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP,			--  8 Bytes
	BultoEnvasado_MdDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP			--  8 Bytes
);																			-- Total: 83 Bytes

CREATE TABLE ROL (
	Rol_Id VARCHAR(15) NOT NULL PRIMARY KEY,			--  15 Bytes
	Rol_Dsc VARCHAR(100),								-- 100 Bytes
	Rol_Fnc VARCHAR(100),								-- 100 Bytes
	Rol_CrDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP,	--   8 Bytes
	Rol_MdDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP	--   8 Bytes
);														-- Total: 231 Bytes

CREATE TABLE USUARIO (
	Usuario_Id VARCHAR(15) NOT NULL PRIMARY KEY,			--  15 Bytes
	Usuario_Nb VARCHAR(50),									--  50 Bytes
	Usuario_Psw VARCHAR(50),								--  50 Bytes
	Usuario_Eml VARCHAR(100),								-- 100 Bytes
	Rol_Id VARCHAR(15) references ROL(Rol_Id) NOT NULL,		--  15 Bytes
	Usuario_CrDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP,	--   8 Bytes
	Usuario_MdDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP	--   8 Bytes
);															-- Total: 246 Bytes

CREATE TABLE COMBINACION (
	Combinacion_Id VARCHAR(15) NOT NULL PRIMARY KEY,							-- 15 Bytes
	ProductoOrigen_Id VARCHAR(15) references PRODUCTO(Producto_Id) NOT NULL,	-- 15 Bytes
	ProductoDestino_Id VARCHAR(15) references PRODUCTO(Producto_Id) NOT NULL,	-- 15 Bytes
	Combinacion_Cnt SMALLINT,													--  2 Bytes
	Combinacion_CrDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP,					--  8 Bytes
	Combinacion_MdDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP					--  8 Bytes
);																				-- Total: 63 Bytes

CREATE TABLE DETALLEOC (
	DetalleOC_Id VARCHAR(15) NOT NULL PRIMARY KEY,								-- 15 Bytes
	OrdenCompra_Id VARCHAR(15) references ORDENCOMPRA(OrdenCompra_Id) NOT NULL,	-- 15 Bytes
	Producto_Id VARCHAR(15) references PRODUCTO(Producto_Id) NOT NULL,			-- 15 Bytes
	DetalleOC_Prc INTEGER,														--  4 Bytes
	DetalleOC_Cnt SMALLINT,														--  2 Bytes
	DetalleOC_DsCnt INTEGER,													--  4 Bytes
	DetalleOC_Tot INTEGER,														--  4 Bytes
	DetalleOC_CrDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP,					--  8 Bytes
	DetalleOC_MdDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP					--  8 Bytes
);																				-- Total: 75 Bytes

CREATE TABLE DETALLELISTA (
	DetalleLista_Id VARCHAR(10),															-- 15 Bytes
	ListaSolicitud_Id VARCHAR(15) references LISTASOLICITUD(ListaSolicitud_Id) NOT NULL,	-- 15 Bytes
	Producto_Id VARCHAR(15) references PRODUCTO(Producto_Id) NOT NULL,						-- 15 Bytes
	Producto_Cnt SMALLINT,																	--  2 Bytes
	DetalleLista_CrDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP,							--  8 Bytes
	DetalleLista_MdDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP								--  8 Bytes
);																							-- Total: 63 Bytes 

CREATE TABLE DETALLEBE (
	DetalleBE_Id VARCHAR(15) NOT NULL PRIMARY KEY,										-- 15 Bytes
	BultoEnvasado_Id VARCHAR(15) references BULTOENVASADO(BultoEnvasado_Id) NOT NULL,	-- 15 Bytes
	Bulto_Id VARCHAR(15) references BULTO(Bulto_Id) NOT NULL,							-- 15 Bytes
	DetalleBE_Cnt INTEGER,																--  4 Bytes
	DetalleBE_CrDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP,							--  8 Bytes
	DetalleBE_MdDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP							--  8 Bytes
);																						-- Total: 65 Bytes

CREATE TABLE RESERVA (
	Reserva_Id VARCHAR(15) NOT NULL PRIMARY KEY,						-- 15 Bytes
	Reserva_Dt TIMESTAMP,												--  8 Bytes
	Reserva_Cnt SMALLINT,												--  2 Bytes
	Reserva_Org VARCHAR(10),											-- 10 Bytes
	Reserva_IdOrg VARCHAR(15),											-- 15 Bytes
	Producto_Id VARCHAR(15) references PRODUCTO(Producto_Id) NOT NULL,	-- 15 Bytes
	Reserva_CrDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP,				--  8 Bytes
	Reserva_MdDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP				--  8 Bytes
);																		-- Total: 81 Bytes

-- Pedro
CREATE TABLE CLIENTE (
	Cliente_Id VARCHAR(15) NOT NULL PRIMARY KEY,			--  15 Bytes
	Cliente_Rut VARCHAR(13),								--  13 Bytes
	Cliente_Nmb VARCHAR(20),								--  20 Bytes
	Cliente_Apll VARCHAR(20),								--  20 Bytes
	Cliente_Eml VARCHAR(100),								-- 100 Bytes
	Cliente_Tel VARCHAR(15),								--  15 Bytes
	Cliente_Dir VARCHAR(100),								-- 100 Bytes
	Cliente_CrDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP,	--   8 Bytes
	Cliente_MdDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP	--   8 Bytes
);															-- Total: 299 Bytes

CREATE TABLE BACKORDER (
	Backorder_Id VARCHAR(15) NOT NULL PRIMARY KEY,					-- 15 Bytes
	Backorder_Dt TIMESTAMP,											--  8 Bytes
	Cliente_Id VARCHAR(15) references CLIENTE(Cliente_Id) NOT NULL,	-- 15 Bytes
	Backorder_CrDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP,		--  8 Bytes
	Backorder_MdDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP		--  8 Bytes
);																	-- Total: 54 Bytes

CREATE TABLE PEDIDO (
	Pedido_Id VARCHAR(15) NOT NULL PRIMARY KEY,						-- 15 Bytes
	Pedido_Dt TIMESTAMP,											--  8 Bytes
	Cliente_Id VARCHAR(15) references CLIENTE(Cliente_Id) NOT NULL,	-- 15 Bytes
	Pedido_CrDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP,			--  8 Bytes
	Pedido_MdDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP			--  8 Bytes
);																	-- Total: 54 Bytes

CREATE TABLE DETALLEBO (
	DetalleBO_Id VARCHAR(15) NOT NULL PRIMARY KEY,							-- 15 Bytes
	DetalleBO_Tot INTEGER,													--  4 Bytes
	DetalleBO_LimDt TIMESTAMP,												--  8 Bytes
	DetalleBO_Cnt SMALLINT,													--  2 Bytes
	Producto_Id VARCHAR(15) references PRODUCTO(Producto_Id) NOT NULL,		-- 15 Bytes
	Backorder_Id VARCHAR(15) references BACKORDER(Backorder_Id) NOT NULL,	-- 15 Bytes
	DetalleBO_CrDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP,				--  8 Bytes
	DetalleBO_MdDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP				--  8 Bytes
);																			-- Total: 75 Bytes

CREATE TABLE DETALLEPEDIDO (
	DetallePedido_Id VARCHAR(15) NOT NULL PRIMARY KEY,					-- 15 Bytes
	DetallePedido_Tot INTEGER,											--  4 Bytes
	DetallePedido_Cnt SMALLINT,											--  2 Bytes
	Producto_Id VARCHAR(15) references PRODUCTO(Producto_Id) NOT NULL,	-- 15 Bytes
	Pedido_Id VARCHAR(15) references PEDIDO(Pedido_Id) NOT NULL,		-- 15 Bytes
	DetallePedido_CrDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP,		--  8 Bytes
	DetallePedido_MdDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP		--  8 Bytes
);																		-- Total: 67 Bytes

CREATE TABLE FORMAPAGO (
	FormaPago_Id VARCHAR(15) NOT NULL PRIMARY KEY,				-- 15 Bytes
	FormaPago_Dsc VARCHAR(20),									-- 20 Bytes
	FormaPago_CrDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP,	--  8 Bytes
	FormaPago_MdDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP	--  8 Bytes
);																-- Total: 51 Bytes

CREATE TABLE VENTA (
	Venta_Id VARCHAR(15) NOT NULL PRIMARY KEY,											-- 15 Bytes
	Venta_CnlDt TIMESTAMP,																--  8 Bytes
	DetallePedido_Id VARCHAR(15) references DetallePedido(DetallePedido_Id) NOT NULL,	-- 15 Bytes
	DetalleBO_Id VARCHAR(15) references DETALEBO(DetalleBO_Id) NOT NULL,				-- 15 Bytes
	FormaPago_Id VARCHAR(15) references FORMAPAGO(FormaPago_Id) NOT NULL,				-- 15 Bytes
	Venta_CrDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP,								--  8 Bytes
	Venta_MdDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP								--  8 Bytes
);																						-- Total: 84 Bytes

CREATE TABLE TIPODEVOLUCION (
	TipoDevolucion_Id VARCHAR(15) NOT NULL PRIMARY KEY,				-- 15 Bytes
	TipoDevolucion_Dsc VARCHAR(20),									-- 20 Bytes
	TipoDevolucion_CrDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP,	--  8 Bytes
	TipoDevolucion_MdDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP	--  8 Bytes
);																	-- Total: 51 Bytes

CREATE TABLE DEVOLUCION (
	Devolucion_Id VARCHAR(15) NOT NULL PRIMARY KEY,											--  15 Bytes
	Devolucion_Rz VARCHAR(100),																-- 100 Bytes
	Devolucion_Mnt INTEGER,																	--   4 Bytes
	Devolucion_Dsc VARCHAR(100),															-- 100 Bytes
	Venta_Id VARCHAR(15) references VENTA(Venta_Id) NOT NULL,								--  15 Bytes
	TipoDevolucion_Id VARCHAR(15) references TIPODEVOLUCION(TipoDevolucion_Id) NOT NULL,	--  15 Bytes
	Devolucion_CrDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP,								--   8 Bytes
	Devolucion_MdDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP								--   8 Bytes
);																							-- Total: 265 Bytes

-- Rodrigo
CREATE TABLE DESPACHO (
	Despacho_Id VARCHAR(15) NOT NULL PRIMARY KEY,
	-- Columnas --
	Despacho_CrDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP,
	Despacho_MdDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP
);

CREATE TABLE GUIADESPACHO (
	GuiaDespacho_Id VARCHAR(15) NOT NULL PRIMARY KEY,
	-- Columnas --
	GuiaDespacho_CrDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP,
	GuiaDespacho_MdDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP
);

CREATE TABLE PRIORIDAD (
	Prioridad_Id VARCHAR(15) NOT NULL PRIMARY KEY,
	-- Columnas --
	Prioridad_CrDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP,
	Prioridad_MdDt TIMESTAMP NOT NULL DEFAULT LOCALTIMESTAMP
);
CREATE OR REPLACE crearListaSolicitud(tipo VARCHAR(10)) 
AS $$
DECLARE
	id VARCHAR(15) := '';
	count_filas INTEGER := 0;
BEGIN
	SELECT COUNT(*) INTO count_filas FROM LISTASOLICITUD;
	id := generarId((count_filas + 1), 3);

	INSERT INTO LISTASOLICITUD VALUES(id, tipo);
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE crearDetalleLista(id_ls VARCHAR(15), 
									id_prod VARCHAR(15), 
									cnt SMALLINT) 
AS $$
DECLARE
	id VARCHAR(15) := '';
	count_filas INTEGER := 0;
BEGIN
	SELECT COUNT(*) INTO count_filas FROM DETALLELISTA;
	id := generarId((count_filas + 1), 4);

	INSERT INTO DETALLELISTA VALUES(id, id_ls, id_prod, cnt);
END;
$$ LANGUAGE plpgsql;
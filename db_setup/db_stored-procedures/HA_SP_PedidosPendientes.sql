CREATE OR REPLACE FUNCTION comprobarPedidosPendientes(id_prod VARCHAR(15))
RETURN 
AS $$
DECLARE
	pendiente := 0;
BEGIN
	SELECT COUNT(*) INTO pendiente
	FROM ORDENCOMPRA AS OC 
	INNER JOIN DETALLEOC AS DOC 
	ON (OC.OrdenCompra_Id = DOC.OrdenCompra_Id) 
	WHERE OC.OrdenCompra_Est != 'COMPLETADO';

	RETURN pendiente;
END
$$ LANGUAGE plpgsql; 
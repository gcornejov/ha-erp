CREATE OR REPLACE PROCEDURE crearProducto(cod_venta VARCHAR(5), 
										cod_madre VARCHAR(10), 
										descrip VARCHAR(50), 
										formato VARCHAR(5), 
										precio INTEGER, 
										pt_pedido SMALLINT, 
										stk_reserva SMALLINT) 
AS $$
DECLARE
	id VARCHAR(15) := '';
	count_filas INTEGER := 0;
BEGIN

	SELECT COUNT(*) INTO count_filas FROM PRODUCTO;
	id := generarId((count_filas + 1), 1);

	INSERT INTO PRODUCTO VALUES(id, cod_venta, cod_madre, descrip, formato, precio, pt_pedido, stk_reserva);

END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE PROCEDURE modificarProducto_CodigoVenta(prod_id VARCHAR(15),
														cod_venta VARCHAR(5), 
														multi_call boolean DEFAULT false) 
AS $$
BEGIN
	IF multi_call THEN
		UPDATE PRODUCTO SET Producto_Cd = cod_venta WHERE Producto_Id = prod_id;
	ELSE
		UPDATE PRODUCTO SET Producto_Cd = cod_venta, Producto_MdDt = LOCALTIMESTAMP WHERE Producto_Id = prod_id;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE PROCEDURE modificarProducto_CodigoMadre(prod_id VARCHAR(15),
														cod_madre VARCHAR(5)
														multi_call boolean DEFAULT false) 
AS $$
BEGIN
	IF multi_call THEN
		UPDATE PRODUCTO SET Producto_CdMd = cod_madre WHERE Producto_Id = prod_id;
	ELSE
		UPDATE PRODUCTO SET Producto_CdMd = cod_madre, Producto_MdDt = LOCALTIMESTAMP WHERE Producto_Id = prod_id;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE PROCEDURE modificarProducto_Descripcion(prod_id VARCHAR(15),
														descrip VARCHAR(5), 
														multi_call boolean DEFAULT false) 
AS $$
BEGIN
	IF multi_call THEN
		UPDATE PRODUCTO SET Producto_Dsc descrip WHERE Producto_Id = prod_id;
	ELSE
		UPDATE PRODUCTO SET Producto_Dsc descrip, Producto_MdDt = LOCALTIMESTAMP WHERE Producto_Id = prod_id;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE PROCEDURE modificarProducto_Formato(prod_id VARCHAR(15),
													formato VARCHAR(5), 
													multi_call boolean DEFAULT false) 
AS $$
BEGIN
	IF multi_call THEN
		UPDATE PRODUCTO SET Producto_Fmt = formato WHERE Producto_Id = prod_id;
	ELSE
		UPDATE PRODUCTO SET Producto_Fmt = formato, Producto_MdDt = LOCALTIMESTAMP WHERE Producto_Id = prod_id;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE PROCEDURE modificarProducto_Precio(prod_id VARCHAR(15),
													precio VARCHAR(5), 
													multi_call boolean DEFAULT false) 
AS $$
BEGIN
	IF multi_call THEN
		UPDATE PRODUCTO SET Producto_Prc = precio WHERE Producto_Id = prod_id;
	ELSE
		UPDATE PRODUCTO SET Producto_Prc = precio, Producto_MdDt = LOCALTIMESTAMP WHERE Producto_Id = prod_id;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE PROCEDURE modificarProducto_PuntoPedido(prod_id VARCHAR(15),
														pt_pedido VARCHAR(5), 
														multi_call boolean DEFAULT false) 
AS $$
DECLARE
	stk SMALLINT := 0;
BEGIN
	SELECT Producto_StRv INTO stk FROM PRODUCTO WHERE Producto_Id = prod_id;

	IF stk < pt_pedido THEN
		IF multi_call THEN
			UPDATE PRODUCTO SET Producto_PtPd = pt_pedido WHERE Producto_Id = prod_id;
		ELSE
			UPDATE PRODUCTO SET Producto_PtPd = pt_pedido, Producto_MdDt = LOCALTIMESTAMP WHERE Producto_Id = prod_id;
		END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE PROCEDURE modificarProducto_StockReserva(prod_id VARCHAR(15),
														stk_reserva VARCHAR(5), 
														multi_call boolean DEFAULT false) 
AS $$
DECLARE
	pp SMALLINT := 0;
BEGIN
	SELECT Producto_PtPd INTO pp FROM PRODUCTO WHERE Producto_Id = prod_id;

	IF pp > stk_reserva THEN
		IF multi_call THEN
			UPDATE PRODUCTO SET Producto_StRv = stk_reserva WHERE Producto_Id = prod_id;
		ELSE
			UPDATE PRODUCTO SET Producto_StRv = stk_reserva, Producto_MdDt = LOCALTIMESTAMP WHERE Producto_Id = prod_id;
		END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE PROCEDURE modificarProducto(prod_id VARCHAR(15),
											cod_venta VARCHAR(5), 
											cod_madre VARCHAR(10), 
											descrip VARCHAR(50), 
											formato VARCHAR(5), 
											precio INTEGER, 
											pt_pedido SMALLINT, 
											stk_reserva SMALLINT) 
AS $$
DECLARE
	-- str_col VARCHAR := '';
	mod BOOLEAN := false;
BEGIN
	IF cod_venta IS NOT NULL AND cod_venta != '' THEN
		-- str_col := str_col || 'Producto_Cd = ''' || cod_venta || ''', ';
		CALL modificarProducto_CodigoVenta(prod_id, cod_venta);
		mod BOOLEAN := true;
	END IF;
	IF cod_madre IS NOT NULL AND cod_madre != '' THEN
		-- str_col := str_col || 'Producto_CdMd = ''' || cod_madre || ''', ';
		modificarProducto_CodigoMadre(prod_id, cod_madre);
		mod BOOLEAN := true;
	END IF;
	IF descrip IS NOT NULL AND descrip != '' THEN
		-- str_col := str_col || 'Producto_Dsc = ''' || descrip || ''', ';
		modificarProducto_Descripcion(prod_id, descrip);
		mod BOOLEAN := true;
	END IF;
	IF formato IS NOT NULL AND formato != '' THEN
		-- str_col := str_col || 'Producto_Fmt = ''' || formato || ''', ';
		modificarProducto_Formato(prod_id, formato);
		mod BOOLEAN := true;
	END IF;
	IF precio IS NOT NULL AND precio > 0 THEN
		-- str_col := str_col || 'Producto_Prc = ' || precio || ', ';
		modificarProducto_Precio(prod_id, precio);
		mod BOOLEAN := true;
	END IF;
	IF pt_pedido IS NOT NULL AND pt_pedido > 0 THEN
		-- str_col := str_col || 'Producto_PtPd = ' || pt_pedido || ', ';
		modificarProducto_PuntoPedido(prod_id, pt_pedido);
		mod BOOLEAN := true;
	END IF;
	IF stk_reserva IS NOT NULL AND stk_reserva > 0 THEN
		-- str_col := str_col || 'Producto_StRv = ' || stk_reserva;
		modificarProducto_StockReserva(prod_id, stk_reserva);
		mod BOOLEAN := true;
	END IF;
	
	/*IF str_col != ''; THEN
		str_query := 'UPDATE PRODUCTO SET ' || str_col || ', Producto_MdDt = LOCALTIMESTAMP WHERE id = ''' || prod_id || '''' ;
		EXECUTE str_query;
	END IF;*/

	IF mod THEN
		UPDATE PRODUCTO SET Producto_MdDt = LOCALTIMESTAMP WHERE Producto_Id = prod_id;
	END IF;

END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION consultarProducto() 
RETURNS SETOF PRODUCTO
AS $$
BEGIN
	RETURN QUERY
	SELECT * FROM PRODUCTO;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION consultarProducto_Id(prod_id VARCHAR(15)) 
RETURNS SETOF PRODUCTO
AS $$
BEGIN
	RETURN QUERY
	SELECT * FROM PRODUCTO WHERE Producto_Id LIKE '%' || prod_id || '%';
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION consultarProducto_CodigoVenta(cod_venta VARCHAR(5)) 
RETURNS SETOF PRODUCTO
AS $$
BEGIN
	RETURN QUERY
	SELECT * FROM PRODUCTO WHERE Producto_Cd LIKE '%' || cod_venta || '%';
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION consultarProducto_CodigoMadre(cod_madre VARCHAR(10)) 
RETURNS SETOF PRODUCTO
AS $$
BEGIN
	RETURN QUERY
	SELECT * FROM PRODUCTO WHERE Producto_CdMd LIKE '%' || cod_madre || '%';
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION consultarProducto_Descripción(descrip VARCHAR(50)) 
RETURNS SETOF PRODUCTO
AS $$
BEGIN
	RETURN QUERY
	SELECT * FROM PRODUCTO WHERE Producto_Dsc LIKE '%' || descrip || '%';
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION consultarProducto_Formato(formato VARCHAR(5)) 
RETURNS SETOF PRODUCTO
AS $$
BEGIN
	RETURN QUERY
	SELECT * FROM PRODUCTO WHERE Producto_Fmt LIKE '%' || formato || '%';
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION consultarProducto_Precio(precio INTEGER) 
RETURNS SETOF PRODUCTO
AS $$
BEGIN
	RETURN QUERY
	SELECT * FROM PRODUCTO WHERE Producto_Prc = prod_id;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION consultarProducto_PuntoPedido(pt_pedido SMALLINT) 
RETURNS SETOF PRODUCTO
AS $$
BEGIN
	RETURN QUERY
	SELECT * FROM PRODUCTO WHERE Producto_PtPd = prod_id;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION consultarProducto_StockReserva(stk_reserva SMALLINT) 
RETURNS SETOF PRODUCTO
AS $$
BEGIN
	RETURN QUERY
	SELECT * FROM PRODUCTO WHERE Producto_StRv = prod_id;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION consultarProducto_Creacion(cr_dt TIMESTAMP) 
RETURNS SETOF PRODUCTO
AS $$
BEGIN
	RETURN QUERY
	SELECT * FROM PRODUCTO WHERE Producto_Id = prod_id;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION consultarProducto_Modificacion(md_dt TIMESTAMP) 
RETURNS SETOF PRODUCTO
AS $$
BEGIN
	RETURN QUERY
	SELECT * FROM PRODUCTO WHERE Producto_Id = prod_id;
END;
$$ LANGUAGE plpgsql;
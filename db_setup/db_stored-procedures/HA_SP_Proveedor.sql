CREATE OR REPLACE PROCEDURE crearProveedor(rut VARCHAR(15), 
										rzn_scl VARCHAR(100), 
										direc VARCHAR(100), 
										tel VARCHAR(15), 
										email VARCHAR(100)) 
AS $$
DECLARE
	id VARCHAR(15) := '';
	count_filas INTEGER := 0;
BEGIN

	SELECT COUNT(*) INTO count_filas FROM PROVEEDOR;
	id := generarId((count_filas + 1), 2);

	INSERT INTO PROVEEDOR VALUES(id, rut, rzn_scl, direc, tel, email);

END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE PROCEDURE modificarProveedor_Rut(prov_id VARCHAR(15),
												rut VARCHAR(5), 
												multi_call boolean DEFAULT false) 
AS $$
BEGIN
	IF multi_call THEN
		UPDATE PROVEEDOR SET Proveedor_Rut = rut WHERE Proveedor_Id = prov_id;
	ELSE
		UPDATE PROVEEDOR SET Proveedor_Rut = rut, Proveedor_MdDt = LOCALTIMESTAMP WHERE Proveedor_Id = prov_id;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE PROCEDURE modificarProveedor_RazonSocial(prov_id VARCHAR(15),
														rzn_scl VARCHAR(5), 
														multi_call boolean DEFAULT false) 
AS $$
BEGIN
	IF multi_call THEN
		UPDATE PROVEEDOR SET Proveedor_RnSl = rzn_scl WHERE Proveedor_Id = prov_id;
	ELSE
		UPDATE PROVEEDOR SET Proveedor_RnSl = rzn_scl, Proveedor_MdDt = LOCALTIMESTAMP WHERE Proveedor_Id = prov_id;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE PROCEDURE modificarProveedor_Direccion(prov_id VARCHAR(15),
														direc VARCHAR(5), 
														multi_call boolean DEFAULT false) 
AS $$
BEGIN
	IF multi_call THEN
		UPDATE PROVEEDOR SET Proveedor_Dir = direc WHERE Proveedor_Id = prov_id;
	ELSE
		UPDATE PROVEEDOR SET Proveedor_Dir = direc, Proveedor_MdDt = LOCALTIMESTAMP WHERE Proveedor_Id = prov_id;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE PROCEDURE modificarProveedor_Telefono(prov_id VARCHAR(15),
														tel VARCHAR(5), 
														multi_call boolean DEFAULT false) 
AS $$
BEGIN
	IF multi_call THEN
		UPDATE PROVEEDOR SET Proveedor_Tel = tel WHERE Proveedor_Id = prov_id;
	ELSE
		UPDATE PROVEEDOR SET Proveedor_Tel = tel, Proveedor_MdDt = LOCALTIMESTAMP WHERE Proveedor_Id = prov_id;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE PROCEDURE modificarProveedor_Email(prov_id VARCHAR(15),
													email VARCHAR(5), 
													multi_call boolean DEFAULT false) 
AS $$
BEGIN
	IF multi_call THEN
		UPDATE PROVEEDOR SET Proveedor_Eml = email WHERE Proveedor_Id = prov_id;
	ELSE
		UPDATE PROVEEDOR SET Proveedor_Eml = email, Proveedor_MdDt = LOCALTIMESTAMP WHERE Proveedor_Id = prov_id;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION consultarProveedor() 
RETURNS SETOF PROVEEDOR
AS $$
BEGIN
	RETURN QUERY
	SELECT * FROM PROVEEDOR;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION consultarProveedor_Id(prov_id VARCHAR(15)) 
RETURNS SETOF PROVEEDOR
AS $$
BEGIN
	RETURN QUERY
	SELECT * FROM PROVEEDOR WHERE Proveedor_Id LIKE '%' || prov_id || '%';
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION consultarProveedor_Rut(rut VARCHAR(15)) 
RETURNS SETOF PROVEEDOR
AS $$
BEGIN
	RETURN QUERY
	SELECT * FROM PROVEEDOR WHERE Proveedor_Rut LIKE '%' || rut || '%';
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION consultarProveedor_RazonSocial(raz_scl VARCHAR(100)) 
RETURNS SETOF PROVEEDOR
AS $$
BEGIN
	RETURN QUERY
	SELECT * FROM PROVEEDOR WHERE Proveedor_RnSl LIKE '%' || raz_scl || '%';
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION consultarProveedor_Direccion(direc VARCHAR(100)) 
RETURNS SETOF PROVEEDOR
AS $$
BEGIN
	RETURN QUERY
	SELECT * FROM PROVEEDOR WHERE Proveedor_Dir LIKE '%' || direc || '%';
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION consultarProveedor_Telefono(tel VARCHAR(15)) 
RETURNS SETOF PROVEEDOR
AS $$
BEGIN
	RETURN QUERY
	SELECT * FROM PROVEEDOR WHERE Proveedor_Tel LIKE '%' || tel || '%';
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION consultarProveedor_Email(email VARCHAR(100)) 
RETURNS SETOF PROVEEDOR
AS $$
BEGIN
	RETURN QUERY
	SELECT * FROM PROVEEDOR WHERE Proveedor_Eml LIKE '%' || email || '%';
END;
$$ LANGUAGE plpgsql;
-- Stock actual
CREATE OR REPLACE FUNCTION stockActual()
RETURNS TABLE (Id VARCHAR(15),
				NombreProducto VARCHAR(50),
				PuntoPedido SMALLINT, 
				StockReserva SMALLINT,
				CantidadDisponible VARCHAR(30), 
				FechaVencimiento TIMESTAMP, 
				Ubicaciones VARCHAR)
AS $$
BEGIN
	RETURN QUERY
	SELECT PRODUCTO.Producto_Id AS Id, 
			PRODUCTO.Producto_Dsc AS NombreProducto, 
			PRODUCTO.Producto_PtPd AS PuntoPedido, 
			PRODUCTO.Producto_StRv AS StockReserva, 
			SUM(BULTOS.CantidadActual) AS CantidadDisponible, 
			PRODUCTO.Producto_Fmt AS FORMATO, 
			MIN(BULTOS.FechaVencimiento) AS FechaVencimiento, 
			string_agg(CONCAT(UBICACION.Ubicacion_Pas, '-',  
							UBICACION.Ubicacion_Col, '-',  
							UBICACION.Ubicacion_Fil), ', ') AS Ubicaciones
	FROM 
		((SELECT Producto_Id, 
				Bulto_CntAct AS CantidadActual, 
				Bulto_VncDt AS FechaVencimiento, 
				Ubicacion_Id
		FROM BULTO
		WHERE Bulto_CntAct > 0 AND Bulto_VncDt > LOCALTIMESTAMP) 

		UNION 

		(SELECT Producto_Id, 
				BultoEnvasado_CntAct AS CantidadActual, 
				BultoEnvasado_VncDt AS FechaVencimiento, 
				Ubicacion_Id
		FROM BULTOENVASADO
		WHERE BultoEnvasado_CntAct > 0 AND BultoEnvasado_VncDt > LOCALTIMESTAMP)) AS BULTOS
	INNER JOIN PRODUCTO ON BULTOS.Producto_Id = PRODUCTO.Producto_Id
	INNER JOIN UBICACION ON BULTOS.Ubicacion_Id = UBICACION.Ubicacion_Id
	GROUP BY Producto_Id;
END;
$$ LANGUAGE plpgsql;

-- Productos por vencer
CREATE OR REPLACE FUNCTION stockPorVencer(intervalo VARCHAR(15))
RETURNS TABLE (Id VARCHAR(15),
				NombreProducto VARCHAR(50),
				CantidadActual SMALLINT, 
				FechaVencimiento TIMESTAMP, 
				Ubicaciones VARCHAR)
AS $$
BEGIN
	RETURN QUERY
	SELECT PRODUCTO.Producto_Id AS Id, 
			PRODUCTO.Producto_Dsc AS NombreProducto, 
			PRODUCTO.Producto_PtPd AS PuntoPedido, 
			PRODUCTO.Producto_StRv AS StockReserva, 
			SUM(BULTOS.CantidadActual) AS CantidadActual, 
			MIN(BULTOS.FechaVencimiento) AS FechaVencimiento, 
			string_agg(CONCAT(UBICACION.Ubicacion_Pas, '-',  
							UBICACION.Ubicacion_Col, '-',  
							UBICACION.Ubicacion_Fil), ', ') AS Ubicaciones
	FROM 
		((SELECT Producto_Id, 
				Bulto_CntAct AS CantidadActual, 
				Bulto_VncDt AS FechaVencimiento, 
				Ubicacion_Id
		FROM BULTO) 

		UNION 

		(SELECT Producto_Id, 
				BultoEnvasado_CntAct AS CantidadActual, 
				BultoEnvasado_VncDt AS FechaVencimiento, 
				Ubicacion_Id
		FROM BULTOENVASADO)) AS BULTOS
	INNER JOIN PRODUCTO ON BULTOS.Producto_Id = PRODUCTO.Producto_Id
	INNER JOIN UBICACION ON BULTOS.Ubicacion_Id = UBICACION.Ubicacion_Id
	WHERE BULTOS.CantidadActual
		AND BULTOS.CantidadActual > 0
		AND (BULTOS.FechaVencimiento - interval '1 month') <= LOCALTIMESTAMP;
END;
$$ LANGUAGE plpgsql;
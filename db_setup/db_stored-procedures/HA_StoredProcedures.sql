CREATE OR REPLACE FUNCTION generarId(id integer, 
									entidad integer, 
									OUT id_generada VARCHAR(15)) 
AS $$
DECLARE
	prefix VARCHAR(3) := '';
BEGIN
	SELECT CodigoEntidad_Txt INTO prefix FROM CODIGOENTIDAD WHERE CodigoEntidad_Id = entidad;
	id_generada := CONCAT_WS('-', prefix, id);
END;
$$ LANGUAGE plpgsql;

-- Mantención de Productos
CREATE OR REPLACE PROCEDURE crearProducto(cod_venta VARCHAR(5), 
										cod_madre VARCHAR(10), 
										descrip VARCHAR(50), 
										formato VARCHAR(5), 
										precio INTEGER, 
										pt_pedido SMALLINT, 
										stk_reserva SMALLINT) 
AS $$
DECLARE
	id VARCHAR(15) := '';
BEGIN

	SELECT COUNT(*) INTO count_filas FROM PRODUCTO;
	id := generarId(count_filas, 1);

	INSERT INTO PRODUCTO VALUES(id, cod_venta, cod_madre, descrip, formato, precio, pt_pedido, stk_reserva);

END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE PROCEDURE modificarProducto_CodigoVenta(prod_id VARCHAR(15),
														cod_venta VARCHAR(5)) 
AS $$
BEGIN
	UPDATE PRODUCTO SET Producto_Cd = cod_venta, Producto_MdDt = LOCALTIMESTAMP WHERE Producto_Id = id;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE PROCEDURE modificarProducto_CodigoMadre(prod_id VARCHAR(15),
														cod_madre VARCHAR(5)) 
AS $$
BEGIN
	UPDATE PRODUCTO SET Producto_CdMd = cod_madre, Producto_MdDt = LOCALTIMESTAMP WHERE Producto_Id = id;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE PROCEDURE modificarProducto_Descripcion(descrip VARCHAR(15),
														descrip VARCHAR(5)) 
AS $$
BEGIN
	UPDATE PRODUCTO SET Producto_Dsc descrip, Producto_MdDt = LOCALTIMESTAMP WHERE Producto_Id = id;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE PROCEDURE modificarProducto_Formato(prod_id VARCHAR(15),
													formato VARCHAR(5)) 
AS $$
BEGIN
	UPDATE PRODUCTO SET Producto_Fmt = formato, Producto_MdDt = LOCALTIMESTAMP WHERE Producto_Id = id;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE PROCEDURE modificarProducto_Precio(prod_id VARCHAR(15),
													precio VARCHAR(5)) 
AS $$
BEGIN
	UPDATE PRODUCTO SET Producto_Prc = precio, Producto_MdDt = LOCALTIMESTAMP WHERE Producto_Id = id;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE PROCEDURE modificarProducto_PuntoPedido(prod_id VARCHAR(15),
														pt_pedido VARCHAR(5)) 
AS $$
DECLARE
	stk SMALLINT := 0;
BEGIN
	SELECT Producto_StRv INTO stk FROM PRODUCTO WHERE Producto_Id = prod_id;

	IF stk < pt_pedido THEN
		UPDATE PRODUCTO SET Producto_PtPd = pt_pedido, Producto_MdDt = LOCALTIMESTAMP WHERE Producto_Id = id;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE PROCEDURE modificarProducto_StockCritico(prod_id VARCHAR(15),
														stk_reserva VARCHAR(5)) 
AS $$
DECLARE
	pp SMALLINT := 0;
BEGIN
	SELECT Producto_PtPd INTO pp FROM PRODUCTO WHERE Producto_Id = prod_id;

	IF pp > stk_reserva THEN
	UPDATE PRODUCTO SET Producto_StRv = stk_reserva, Producto_MdDt = LOCALTIMESTAMP WHERE Producto_Id = id;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE PROCEDURE modificarProducto(prod_id VARCHAR(15),
											cod_venta VARCHAR(5), 
											cod_madre VARCHAR(10), 
											descrip VARCHAR(50), 
											formato VARCHAR(5), 
											precio INTEGER, 
											pt_pedido SMALLINT, 
											stk_reserva SMALLINT) 
AS $$
DECLARE
	-- str_col VARCHAR := '';
BEGIN
	IF cod_venta IS NOT NULL AND cod_venta != '' THEN
		-- str_col := str_col || 'Producto_Cd = ''' || cod_venta || ''', ';
		CALL modificarProducto_CodigoVenta(prod_id, cod_venta);
	END IF;
	IF cod_madre IS NOT NULL AND cod_madre != '' THEN
		-- str_col := str_col || 'Producto_CdMd = ''' || cod_madre || ''', ';
		modificarProducto_CodigoMadre(prod_id, cod_madre);
	END IF;
	IF descrip IS NOT NULL AND descrip != '' THEN
		-- str_col := str_col || 'Producto_Dsc = ''' || descrip || ''', ';
		modificarProducto_Descripcion(prod_id, descrip);
	END IF;
	IF formato IS NOT NULL AND formato != '' THEN
		-- str_col := str_col || 'Producto_Fmt = ''' || formato || ''', ';
		modificarProducto_Formato(prod_id, formato);
	END IF;
	IF precio IS NOT NULL AND precio > 0 THEN
		-- str_col := str_col || 'Producto_Prc = ' || precio || ', ';
		modificarProducto_Precio(prod_id, precio);
	END IF;
	IF pt_pedido IS NOT NULL AND pt_pedido > 0 THEN
		-- str_col := str_col || 'Producto_PtPd = ' || pt_pedido || ', ';
		modificarProducto_PuntoPedido(prod_id, pt_pedido);
	END IF;
	IF stk_reserva IS NOT NULL AND stk_reserva > 0 THEN
		-- str_col := str_col || 'Producto_StRv = ' || stk_reserva;
		modificarProducto_StockCritico(prod_id, stk_reserva);
	END IF;
	
	/*IF str_col != ''; THEN
		str_query := 'UPDATE PRODUCTO SET ' || str_col || ', Producto_MdDt = LOCALTIMESTAMP WHERE id = ''' || prod_id || '''' ;
		EXECUTE str_query;
	END IF;*/
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION consultarProducto() 
RETURNS SETOF PRODUCTO
AS $$
BEGIN
	SELECT * FROM PRODUCTO;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION consultarProducto_Id(prod_id VARCHAR(15)) 
RETURNS SETOF PRODUCTO
AS $$
BEGIN
	SELECT * FROM PRODUCTO WHERE Producto_Id = prod_id;
END;
$$ LANGUAGE plpgsql;